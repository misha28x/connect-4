export * from './lib/board';
export * from './lib/figure';
export * from './lib/player';
export * from './lib/game';
