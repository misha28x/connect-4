import { Board, WinnSequence } from './board';
import { FigureColor, FigureColors } from './figure';

const ROWS = 6;
const COLS = 7;

const LAST_ROW = ROWS - 1;
function createBoardWithDefaultSize() {
  return new Board(COLS, ROWS);
}

describe('Board', () => {
  it('should correctly detect cell owner', () => {
    const board = createBoardWithDefaultSize();
    const figure = FigureColors.yellow;

    board.placeFigure(1, figure);

    expect(board.getCellFigure(1, LAST_ROW)).toBe(figure);
  });

  it('should correctly place a figure', () => {
    const board = createBoardWithDefaultSize();
    const figure = FigureColors.yellow;

    board.placeFigure(0, figure);

    expect(board.getCellFigure(0, LAST_ROW)).toBe(figure);
  });

  it('should detect if turn is unavailable', function () {
    const board = createBoardWithDefaultSize();
    const figure: FigureColor = FigureColors.red;

    for (let i = 0; i < ROWS; i++) {
      board.placeFigure(1, figure);
    }

    expect(board.isTurnAvailable(1)).toBeFalsy();
  });

  describe('should correctly detect closest col', () => {
    it('if col is available', function () {
      const board = createBoardWithDefaultSize();
      const expectedCol = 3;

      expect(board.getClosestCol(expectedCol)).toBe(expectedCol);
    });

    it('if col is full and left is available', function () {
      const board = createBoardWithDefaultSize();
      const expectedCol = 0;

      for (let i = 0; i < ROWS; i++) {
        board.placeFigure(1, FigureColors.red);
      }

      expect(board.getClosestCol(expectedCol)).toBe(expectedCol);
    });

    it('if col is full and left is unavailable', function () {
      const board = createBoardWithDefaultSize();
      const expectedCol = 1;

      for (let i = 0; i < ROWS; i++) {
        board.placeFigure(0, FigureColors.red);
      }

      expect(board.getClosestCol(expectedCol)).toBe(expectedCol);
    });
  });

  describe('should correctly detect winner', () => {
    it('vertically', function () {
      const board = createBoardWithDefaultSize();
      const figure: FigureColor = FigureColors.red;
      const expectedSequence: WinnSequence = [
        [0, 2],
        [0, 3],
        [0, 4],
        [0, 5],
      ];

      for (let i = 0; i < 4; i++) {
        board.placeFigure(0, figure);
      }

      expect(board.checkForWinner(0)).toEqual(expectedSequence);
    });

    it('horizontally', function () {
      const board = createBoardWithDefaultSize();
      const figure = FigureColors.yellow;
      const expectedSequence: WinnSequence = [
        [0, 5],
        [1, 5],
        [2, 5],
        [3, 5],
      ];

      board.placeFigure(0, figure);
      board.placeFigure(1, figure);
      board.placeFigure(3, figure);
      board.placeFigure(2, figure);

      expect(board.checkForWinner(2)).toEqual(expectedSequence);
    });

    it('on the last row', function () {
      const board = createBoardWithDefaultSize();
      const figure = FigureColors.yellow;
      const figure2 = FigureColors.red;
      const expectedSequence: WinnSequence = [
        [0, 0],
        [0, 1],
        [0, 2],
        [0, 3],
      ];

      // placing figures so the winning figure would be at last row
      board.placeFigure(0, figure);
      board.placeFigure(0, figure);
      board.placeFigure(0, figure2);
      board.placeFigure(0, figure2);
      board.placeFigure(0, figure2);
      board.placeFigure(0, figure2);

      expect(board.checkForWinner(0)).toEqual(expectedSequence);
    });

    it('diagonally', function () {
      const board = createBoardWithDefaultSize();
      const expectedSequence: WinnSequence = [
        [0, 5],
        [1, 4],
        [2, 3],
        [3, 2],
      ];

      winDiagonally(board);

      expect(board.checkForWinner(0)).toEqual(expectedSequence);
    });
  });

  describe('should correctly detect if there is not enough figures', () => {
    it('vertically', function () {
      const board = createBoardWithDefaultSize();
      const figure = FigureColors.red;

      for (let i = 0; i < 3; i++) {
        board.placeFigure(0, figure);
      }

      expect(board.checkForWinner(0)).toBe(null);
    });

    it('horizontally', function () {
      const board = createBoardWithDefaultSize();
      const figure = FigureColors.yellow;

      board.placeFigure(0, figure);
      board.placeFigure(1, figure);
      board.placeFigure(2, figure);

      expect(board.checkForWinner(0)).toBe(null);
    });
  });

  it('should correctly detect if game over and there is no winner', function () {
    const board = createBoardWithDefaultSize();
    const figure = FigureColors.yellow;
    const figure2 = FigureColors.red;

    let currentPlayer: FigureColor = figure;

    for (let i = 0; i < ROWS; i++) {
      for (let col = 0; col < COLS; col++) {
        board.placeFigure(col, currentPlayer);
        currentPlayer = currentPlayer === figure ? figure2 : figure;
      }
    }

    expect(board.isBoardFull()).toBeTruthy();
  });
});

function winDiagonally(board: Board) {
  const red = FigureColors.red;
  const yellow = FigureColors.yellow;

  board.placeFigure(0, red);
  board.placeFigure(1, yellow);
  board.placeFigure(1, red);
  board.placeFigure(2, yellow);
  board.placeFigure(3, red);
  board.placeFigure(2, yellow);
  board.placeFigure(2, red);
  board.placeFigure(3, yellow);
  board.placeFigure(3, red);
  board.placeFigure(4, yellow);
  board.placeFigure(3, red);
}
