import { Figure, FigureColor } from './figure';

export type BoardCells = (FigureColor | null)[][];
type Pair = [col: number, row: number];
export type WinnSequence = [Pair, Pair, Pair, Pair];

export class Board {
  private _cells: BoardCells = [];
  private _freeCells = 0;

  private figures: Figure[] = [];

  constructor(private columns: number, private rows: number) {
    this.createBoard(columns, rows);
  }

  get cells(): Figure[] {
    return this.figures;
  }

  isTurnAvailable(col: number): boolean {
    return this._cells[0][col] === null;
  }

  isBoardFull(): boolean {
    return this._freeCells === 0;
  }

  placeFigure(col: number, color: FigureColor) {
    const topRowIdx = this.getTopFreeCell(col);

    this._cells[topRowIdx][col] = color;

    this._freeCells--;
    this.figures.push({ row: topRowIdx, col, color });
  }

  getCellFigure(column: number, row: number): FigureColor | null {
    return this._cells[row][column];
  }

  checkForWinner(col: number): WinnSequence | null {
    const lastFiledRowIdx = this.getTopFilledCell(col);
    const color = this.getCellFigure(col, lastFiledRowIdx);

    if (color === null) return null;
    // Todo: Try to rewrite this to DFS
    const sequence =
      this.checkHorizontal(col, lastFiledRowIdx, color) ||
      this.checkVertical(col, lastFiledRowIdx, color) ||
      this.checkDiagonal(col, lastFiledRowIdx, color);

    return sequence ?? null;
  }

  getClosestCol(col: number): number {
    if (this.isTurnAvailable(col)) return col;

    let toLeft = col;
    let toRight = col;

    while (toLeft >= 0 || toRight < this.columns) {
      if (this.isTurnAvailable(--toLeft)) return toLeft;
      if (this.isTurnAvailable(++toRight)) return toRight;
    }

    return -1;
  }

  // we could check only one side as we will always start at top most cell
  private checkVertical(col: number, row: number, color: FigureColor): WinnSequence | null {
    let currentRow = row;
    const seq = [];
    while (currentRow < this.rows && this._cells[currentRow][col] === color) {
      seq.push([col, currentRow]);
      currentRow++;
    }

    return seq.length === 4 ? (seq as WinnSequence) : null;
  }

  private checkHorizontal(col: number, row: number, color: FigureColor): WinnSequence | null {
    const seq = [[col, row]];

    let leftCol = col - 1;
    while (leftCol >= 0 && this._cells[row][leftCol] === color) {
      seq.unshift([leftCol, row]);
      leftCol--;
    }

    let rightCol = col + 1;
    while (rightCol < this.columns && this._cells[row][rightCol] === color) {
      seq.push([rightCol, row]);
      rightCol++;
    }
    return seq.length === 4 ? (seq as WinnSequence) : null;
  }

  private checkDiagonal(col: number, row: number, color: FigureColor): WinnSequence | null {
    let seq = [[col, row]];
    let currentCol = col - 1;
    let currentRow = row - 1;

    // check diagonal to the left-up
    while (currentCol >= 0 && currentRow >= 0 && this._cells[currentRow][currentCol] === color) {
      seq.unshift([currentCol, currentRow]);
      currentCol--;
      currentRow--;
    }

    currentCol = col + 1;
    currentRow = row + 1;

    // check diagonal to the right-down
    while (
      currentCol < this.columns &&
      currentRow < this.rows &&
      this._cells[currentRow][currentCol] === color
    ) {
      seq.push([currentCol, currentRow]);
      currentCol++;
      currentRow++;
    }

    if (seq.length === 4) return seq as WinnSequence;

    seq = [[col, row]];
    currentCol = col - 1;
    currentRow = row + 1;

    // check diagonal to the left-down
    while (
      currentCol >= 0 &&
      currentRow < this.rows &&
      this._cells[currentRow][currentCol] === color
    ) {
      seq.unshift([currentCol, currentRow]);
      currentCol--;
      currentRow++;
    }

    currentCol = col + 1;
    currentRow = row - 1;

    // check diagonal to the right-up
    while (
      currentCol < this.columns &&
      currentRow >= 0 &&
      this._cells[currentRow][currentCol] === color
    ) {
      seq.push([currentCol, currentRow]);
      currentCol++;
      currentRow--;
    }

    if (seq.length === 4) return seq as WinnSequence;

    return null;
  }

  private getTopFilledCell(col: number): number {
    return this.getTopFreeCell(col) + 1;
  }

  private getTopFreeCell(column: number): number {
    if (!this.isTurnAvailable(column)) return -1;

    let rowIdx = this.rows - 1;

    while (this._cells[rowIdx][column] !== null && rowIdx >= 0) {
      rowIdx--;
    }

    return rowIdx;
  }

  private createBoard(cols: number, rows: number) {
    this._freeCells = cols * rows;
    for (let i = 0; i < rows; i++) {
      this._cells[i] = new Array(cols).fill(null);
    }
  }
}
