import { WinnSequence } from './board';

export const FigureColors = {
  red: 'red',
  yellow: 'yellow',
} as const;

export const availableColors = Object.values(FigureColors);
export type FigureColor = (typeof availableColors)[number];

export type Figure = {
  row: number;
  col: number;
  color: FigureColor;
};

export function isPartOfSequence(figure: Figure, sequence: WinnSequence) {
  return sequence.some(([c, r]) => c === figure.col && r === figure.row);
}
