import { COLS, Game, ROWS, Winner } from './game';

describe('Game', () => {
  test('should start game with first player', () => {
    const game = new Game();
    const [player1] = game.getPlayers();

    expect(game.currentPlayer).toBe(player1);
  });

  test('should change turn correctly', () => {
    const game = new Game();
    const [, player2] = game.getPlayers();

    game.placeFigure(0);

    expect(game.currentPlayer).toBe(player2);
  });

  test('should detect winner', () => {
    const game = new Game();
    const [player1] = game.getPlayers();
    const winner: Winner = {
      player: player1,
      seq: [
        [0, 2],
        [0, 3],
        [0, 4],
        [0, 5],
      ],
    };

    winWithFirstPlayer(game);

    expect(game.ended).toBe(true);
    expect(game.winner).toEqual(winner);
  });

  test('should detect tie', () => {
    const game = new Game();

    playATie(game);

    expect(game.ended).toBe(true);
    expect(game.winner).toEqual(null);
  });
});

function winWithFirstPlayer(game: Game) {
  game.placeFigure(0);
  game.placeFigure(1);
  game.placeFigure(0);
  game.placeFigure(2);
  game.placeFigure(0);
  game.placeFigure(2);
  game.placeFigure(0);
}

function playATie(game: Game) {
  // Yeah, those were recorded from played game, I'm not writing logic to play tie
  game.placeFigure(0);
  game.placeFigure(0);
  game.placeFigure(1);
  game.placeFigure(1);
  game.placeFigure(2);
  game.placeFigure(2);
  game.placeFigure(6);
  game.placeFigure(6);
  game.placeFigure(5);
  game.placeFigure(5);
  game.placeFigure(4);
  game.placeFigure(4);
  game.placeFigure(6);
  game.placeFigure(6);
  game.placeFigure(5);
  game.placeFigure(5);
  game.placeFigure(4);
  game.placeFigure(4);
  game.placeFigure(6);
  game.placeFigure(6);
  game.placeFigure(5);
  game.placeFigure(5);
  game.placeFigure(4);
  game.placeFigure(4);
  game.placeFigure(0);
  game.placeFigure(0);
  game.placeFigure(1);
  game.placeFigure(1);
  game.placeFigure(2);
  game.placeFigure(3);
  game.placeFigure(3);
  game.placeFigure(3);
  game.placeFigure(3);
  game.placeFigure(3);
  game.placeFigure(3);
  game.placeFigure(2);
  game.placeFigure(2);
  game.placeFigure(2);
  game.placeFigure(1);
  game.placeFigure(1);
  game.placeFigure(0);
  game.placeFigure(0);
}
