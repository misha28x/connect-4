import { createPlayer, Player } from './player';
import { Board, WinnSequence } from './board';

export const COLS = 7;
export const ROWS = 6;

type GameEndListener = (winner: Winner | null) => void;
type TurnChangeListener = (player: Player) => void;

export type Winner = { seq: WinnSequence; player: Player };

export class Game {
  private _currentPlayer!: Player;
  private board!: Board;
  winner: Winner | null = null;

  get figures() {
    return this.board.cells;
  }

  get currentPlayer() {
    return this._currentPlayer;
  }

  private set currentPlayer(player: Player) {
    this._currentPlayer = player;
    this.turnChanged();
  }

  get ended() {
    return Boolean(this.winner || this.board.isBoardFull());
  }

  private gameEndListeners: GameEndListener[] = [];
  private turnListeners: TurnChangeListener[] = [];

  constructor(
    private player1 = createPlayer('Player 1'),
    private player2 = createPlayer('Player 2')
  ) {
    this._currentPlayer = this.player1;
    this.initBoard();
  }

  playAgain() {
    this.restart();
    this.changeTurn();
  }

  restart() {
    const starter = this.getStarter();
    this.initBoard();
    this.currentPlayer = starter;
  }

  getPlayers() {
    return [this.player1, this.player2];
  }

  placeFigure(col: number) {
    if (this.ended) return;

    const colIdx = this.board.getClosestCol(col);

    this.board.placeFigure(colIdx, this.currentPlayer.color);
    const sequence = this.board.checkForWinner(colIdx);

    if (sequence != null) {
      return this.endGame(sequence);
    }

    if (this.board.isBoardFull()) {
      return this.gameEndListeners.forEach((cb) => cb(null));
    }

    this.changeTurn();
  }

  onGameEnd(cb: GameEndListener) {
    this.gameEndListeners.push(cb);
  }

  private getStarter(): Player {
    const firstTurn = this.board.cells[0];

    if (firstTurn == null) {
      return this.currentPlayer;
    }

    return firstTurn.color === 'red' ? this.player1 : this.player2;
  }

  private endGame(seq: WinnSequence) {
    this.currentPlayer.won();
    this.winner = { seq, player: this.currentPlayer };
    this.gameEndListeners.forEach((cb) => cb(this.winner));
  }

  private initBoard() {
    this.winner = null;
    this.board = new Board(COLS, ROWS);
  }

  private turnChanged() {
    this.turnListeners.forEach((cb) => cb(this.currentPlayer));
  }

  private changeTurn() {
    this.currentPlayer = this.currentPlayer === this.player1 ? this.player2 : this.player1;
  }
}
