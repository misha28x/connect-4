import { availableColors, FigureColor } from './figure';

export class Player {
  constructor(
    public id: string,
    public name: string,
    public color: FigureColor,
    public score = 0
  ) {}

  won() {
    this.score++;
  }
}

let uniqueId = 0;
const figures: FigureColor[] = [...availableColors];

let nextFigure = 0;
function getNextFigureIdx() {
  return nextFigure++ % figures.length;
}

function nextId() {
  return (uniqueId++).toString();
}

export function createPlayer(name: string, id: string = nextId()) {
  const figure = figures[getNextFigureIdx()];
  return new Player(id, name, figure);
}
