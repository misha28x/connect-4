import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';

import { CardComponent } from '../card/card.component';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'app-end-notice',
  standalone: true,
  imports: [CommonModule, CardComponent, ButtonComponent, DialogModule],
  templateUrl: './end-notice.component.html',
  styleUrls: ['./end-notice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EndNoticeComponent {
  dialogRef = inject(DialogRef);

  close() {
    this.dialogRef.close();
  }
}
