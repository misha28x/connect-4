import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EndNoticeComponent } from './end-notice.component';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';

describe('EndNoticeComponent', () => {
  let component: EndNoticeComponent;
  let fixture: ComponentFixture<EndNoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DialogModule, EndNoticeComponent],
      providers: [{ provide: DialogRef, useValue: {} }],
    }).compileComponents();

    fixture = TestBed.createComponent(EndNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
