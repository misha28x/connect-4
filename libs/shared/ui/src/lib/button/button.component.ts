import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

type ButtonColor = 'red' | 'yellow' | 'default';
@Component({
  selector: 'button[app-button], a[app-button]',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input() color: ButtonColor = 'default';

  @HostBinding('attr.type')
  @Input()
  type: 'button' | 'submit' = 'button';

  @HostBinding('class') get hostClasses() {
    return `app-button-${this.color}`;
  }
}
