import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'button[chip-button],a[chip-button]',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './chip-button.component.html',
  styleUrls: ['./chip-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipButtonComponent {
  @HostBinding('attr.role') role = 'button';
  @HostBinding('attr.type') type = 'button';
}
