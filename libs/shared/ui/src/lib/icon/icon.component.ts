import { ChangeDetectionStrategy, Component, HostBinding, inject, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ICON_RESOLVER } from 'shared/tokens';

@Component({
  selector: 'app-icon',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  resolver = inject(ICON_RESOLVER);

  @Input() key = '';
  @Input() description = '';

  @HostBinding('style.height.px')
  @Input()
  size!: number;

  get icon() {
    return this.resolver.resolve(this.key);
  }
}
