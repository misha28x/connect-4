import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'button[icon-button], a[icon-button]',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
})
export class IconButtonComponent {}
