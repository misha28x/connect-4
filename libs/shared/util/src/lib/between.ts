export function between(val: number, min: number, max: number) {
  return val >= min && val <= max;
}
