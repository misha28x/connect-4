import { Player } from 'game';
import type { Figure, Winner } from 'game';

export const ClientMessages = {
  close: 'close',
  col: 'col',
  start: 'start',
  create: 'create',
  join: 'join',
  turn: 'turn',
  restart: 'restart',
  playAgain: 'playAgain',
} as const;

export const ServerMessages = {
  closed: 'closed',
  colUpdate: 'colUpdate',
  created: 'created',
  win: 'win',
  tie: 'tie',
  update: 'update',
  joined: 'joined',
  started: 'started',
} as const;

export type ClientMessageType = keyof typeof ClientMessages;
export type ServerMessageType = keyof typeof ServerMessages;

type BaseMessage<T extends ClientMessageType | ServerMessageType> = {
  type: T;
};

type CreateGame = BaseMessage<'create'>;
type Restart = BaseMessage<'restart'>;
type PlayAgain = BaseMessage<'playAgain'>;
type Start = BaseMessage<'start'>;
type Close = BaseMessage<'close'> & {
  sessionId: string;
};

type JoinGame = BaseMessage<'join'> & {
  sessionId: string;
};

type Turn = BaseMessage<'turn'> & {
  col: number;
};

type Col = BaseMessage<'col'> & { col: number };

type GameCreated = BaseMessage<'created'> & {
  sessionId: string;
};
type Win = BaseMessage<'win'> & { winner: Winner };
type Tie = BaseMessage<'tie'>;
type Closed = BaseMessage<'closed'>;

type Started = BaseMessage<'started'> & { players: Player[]; currentPlayerId: string };
type Joined = BaseMessage<'joined'> & { players: Player[]; sessionId: string };

type Update = BaseMessage<'update'> & {
  currentPlayerId: string;
  figure: Figure | null;
};

type ColUpdate = BaseMessage<'colUpdate'> & {
  cols: [number, number];
};

export type ClientMessage =
  | CreateGame
  | JoinGame
  | Turn
  | Restart
  | PlayAgain
  | Start
  | Col
  | Close;

export type ServerMessage =
  | GameCreated
  | Win
  | Tie
  | Joined
  | Update
  | ColUpdate
  | Started
  | Closed;

export type Message = ClientMessage | ServerMessage;
export type MessageByType<T extends ClientMessageType | ServerMessageType> = Extract<
  Message,
  { type: T }
>;

export type MessagePayload<T extends ClientMessageType | ServerMessageType> = Omit<
  MessageByType<T>,
  'type'
>;

function isMessage(data: unknown): data is Message {
  return typeof data === 'object' && data !== null && 'type' in data;
}

export function isClientMessage(data: unknown): data is ClientMessage {
  if (!isMessage(data)) return false;
  return Object.keys(ClientMessages).includes(data.type);
}

export function isServerMessage(data: unknown): data is ServerMessage {
  if (!isMessage(data)) return false;
  return Object.keys(ServerMessages).includes(data.type);
}
