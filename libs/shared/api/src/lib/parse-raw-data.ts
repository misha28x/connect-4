import { RawData } from 'ws';
import { ClientMessage, isClientMessage } from './message';

export function parseClientMessage(data: RawData): ClientMessage {
  const stringData = data.toString();
  const objectData = JSON.parse(stringData) as unknown;

  if (!isClientMessage(objectData)) {
    throw new Error('Invalid message');
  }

  return objectData;
}
