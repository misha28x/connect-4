import { inject, Pipe, PipeTransform } from '@angular/core';
import { ICON_RESOLVER } from 'shared/tokens';

@Pipe({
  name: 'svgIcon',
  standalone: true,
})
export class SvgIconPipe implements PipeTransform {
  resolver = inject(ICON_RESOLVER);
  transform(key: string | null): string {
    if (!key) return '';

    return this.resolver.resolve(key);
  }
}
