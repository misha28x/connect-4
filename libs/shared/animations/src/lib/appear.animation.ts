import { animate, state, style, transition, trigger } from '@angular/animations';

export const appear = trigger('appear', [
  state(
    'void',
    style({
      transform: 'translateY(0)',
    })
  ),
  transition(':enter', [
    style({ transform: 'translateY(-{{diff}}px)', opacity: 0.5 }),
    animate('500ms', style({ transform: 'translateY(0)' })),
  ]),
]);
