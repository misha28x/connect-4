import { animate, style, transition, trigger } from '@angular/animations';

export const incrementAnimation = trigger('valueAnimation', [
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'translateY(10px)',
    }),
    animate(
      '0.3s 0.2s ease-in-out',
      style({
        opacity: 1,
        transform: 'translateY(0%)',
      })
    ),
  ]),
  transition(':leave', [
    animate(
      '0.3s ease-in-out',
      style({
        opacity: 0,
        transform: 'translateY(-10px)',
      })
    ),
  ]),
]);
