import { inject, InjectionToken } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { distinctUntilChanged, fromEvent, map, startWith } from 'rxjs';

const pageVisibilityFactory = () => {
  const document = inject(DOCUMENT);
  return fromEvent(document, 'visibilitychange').pipe(
    map(() => !document.hidden),
    startWith(!document.hidden),
    distinctUntilChanged()
  );
};

export const PAGE_VISIBILITY = new InjectionToken('Page visibility stream', {
  factory: pageVisibilityFactory,
});
