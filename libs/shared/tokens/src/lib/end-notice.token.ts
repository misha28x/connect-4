import { InjectionToken } from '@angular/core';
import { ComponentType } from '@angular/cdk/overlay';

export const END_NOTICE = new InjectionToken<() => Promise<ComponentType<unknown>>>(
  'Async function that returns end notice modal'
);
