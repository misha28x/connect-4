import { InjectionToken } from '@angular/core';

export type IconResolver = {
  resolve: (key: string) => string;
};

const defaultIconResolver: IconResolver = {
  resolve: (key: string) => {
    return `/assets/images/${key}.svg`;
  },
};

export const ICON_RESOLVER = new InjectionToken('ICON_RESOLVER', {
  factory: () => defaultIconResolver,
});
