import {
  distinctUntilChanged,
  EMPTY,
  interval,
  map,
  Observable,
  scan,
  startWith,
  switchMap,
} from 'rxjs';
import { inject, InjectionToken } from '@angular/core';

import { GameService } from 'connect-four/models';
import { PAGE_VISIBILITY } from './page-visibility';

const TIMER_VALUE = 30;

const TIMER_PRODUCER = new InjectionToken<Observable<number>>('Game timer');

export function provideTimer() {
  return {
    provide: TIMER_PRODUCER,
    useFactory: () => {
      const player$ = inject(GameService).currentPlayer$;
      const pageVisible$ = inject(PAGE_VISIBILITY);

      return player$.pipe(
        switchMap(() =>
          pageVisible$.pipe(
            distinctUntilChanged(),
            switchMap((val) => (val ? interval$().pipe(startWith(0)) : EMPTY)),
            scan((acc, cur) => (cur ? Math.max(0, acc - cur) : acc), TIMER_VALUE)
          )
        ),
        startWith(TIMER_VALUE)
      );
    },
  };
}

export function injectTimer() {
  return inject(TIMER_PRODUCER);
}

function interval$() {
  return interval(1000).pipe(map(() => 1));
}
