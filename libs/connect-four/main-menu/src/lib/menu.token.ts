import { inject, InjectionToken } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable, shareReplay } from 'rxjs';

type Screen = 'default' | 'multiplayer' | 'connect' | 'lobby';

export const MENU_SCREEN = new InjectionToken<Observable<Screen>>('Menu mode');

export function injectMenuScreen() {
  return inject(MENU_SCREEN);
}

export function provideMenuScreen() {
  return {
    provide: MENU_SCREEN,
    useFactory: factory,
  };
}

function factory(): Observable<boolean> {
  const route = inject(ActivatedRoute);
  return route.queryParams.pipe(
    map((p) => p['mode'] ?? 'default'),
    shareReplay(1)
  );
}
