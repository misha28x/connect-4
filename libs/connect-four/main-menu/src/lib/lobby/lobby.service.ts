import { inject, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, distinctUntilChanged, map, merge, startWith } from 'rxjs';

import { MultiplayerGameService } from 'connect-four/services';

@Injectable()
export class LobbyService {
  private route = inject(ActivatedRoute);
  private gameService = inject(MultiplayerGameService);

  private sessionId$ = merge(
    this.gameService.sessionId$,
    this.route.queryParams.pipe(map((p) => p['sessionId']))
  ).pipe(distinctUntilChanged(), startWith(null));

  vm$ = combineLatest({
    sessionId: this.sessionId$,
    players: this.gameService.players$,
  });

  createGame() {
    if (!this.gameAlreadyExists()) {
      this.gameService.createGame();
    }
  }

  startGame() {
    this.gameService.startGame();
  }

  private gameAlreadyExists() {
    const snapshot = this.route.snapshot;
    const params = snapshot.queryParams;
    const sessionId = params['sessionId'];

    return Boolean(sessionId) && sessionId.length > 0;
  }
}
