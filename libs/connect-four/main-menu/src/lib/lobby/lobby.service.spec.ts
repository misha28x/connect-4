import { TestBed } from '@angular/core/testing';

import { LobbyService } from './lobby.service';
import { RouterTestingModule } from '@angular/router/testing';
import { DialogModule } from '@angular/cdk/dialog';
import { END_NOTICE } from 'shared/tokens';

describe('LobbyService', () => {
  let service: LobbyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, DialogModule],
      providers: [LobbyService, { provide: END_NOTICE, useValue: () => null }],
    });
    service = TestBed.inject(LobbyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
