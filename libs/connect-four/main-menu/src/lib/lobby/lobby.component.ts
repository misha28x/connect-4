import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Clipboard } from "@angular/cdk/clipboard";
import { RouterLink } from "@angular/router";

import { ButtonComponent, CardComponent, ChipButtonComponent, IconButtonComponent, IconComponent } from "shared/ui";

import { LobbyService } from "./lobby.service";

@Component({
  selector: 'app-lobby',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    IconComponent,
    IconButtonComponent,
    ButtonComponent,
    ChipButtonComponent,
    RouterLink,
  ],
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss'],
  providers: [LobbyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LobbyComponent implements AfterViewInit {
  lobbySv = inject(LobbyService);
  clipboard = inject(Clipboard);
  cdr = inject(ChangeDetectorRef);

  vm$ = this.lobbySv.vm$;
  copied = false;

  ngAfterViewInit() {
    this.lobbySv.createGame();
  }

  start() {
    this.lobbySv.startGame();
  }

  copy(text: string) {
    this.copied = this.clipboard.copy(text);
    setTimeout(() => {
      this.copied = false;
      this.cdr.markForCheck();
    }, 5000);
  }
}
