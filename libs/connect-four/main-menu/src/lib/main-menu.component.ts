import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';

import { ButtonComponent, CardComponent, IconComponent } from 'shared/ui';
import { createFadeAnimation } from 'shared/animations';

import { LobbyComponent } from './lobby/lobby.component';
import { ConnectionScreenComponent } from './connection-screen/connection-screen.component';
import { injectMenuScreen, provideMenuScreen } from './menu.token';

@Component({
  selector: 'app-main-menu',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    IconComponent,
    ButtonComponent,
    RouterLink,
    ConnectionScreenComponent,
    LobbyComponent,
  ],
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  providers: [provideMenuScreen()],
  animations: [createFadeAnimation({ enterDuration: 250 })],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainMenuComponent {
  menuMode$ = injectMenuScreen();
}
