import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConnectionScreenComponent } from './connection-screen.component';
import { DialogModule } from '@angular/cdk/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { END_NOTICE } from 'shared/tokens';

describe('ConnectionScreenComponent', () => {
  let component: ConnectionScreenComponent;
  let fixture: ComponentFixture<ConnectionScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConnectionScreenComponent, DialogModule, RouterTestingModule],
      providers: [{ provide: END_NOTICE, useValue: () => null }],
    }).compileComponents();

    fixture = TestBed.createComponent(ConnectionScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
