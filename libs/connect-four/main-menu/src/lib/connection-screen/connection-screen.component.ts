import { ChangeDetectionStrategy, Component, inject, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { skip, Subscription } from 'rxjs';
import { ButtonComponent, CardComponent } from 'shared/ui';
import { MultiplayerGameService } from 'connect-four/services';

@Component({
  selector: 'app-connection-screen',
  standalone: true,
  imports: [CommonModule, ButtonComponent, CardComponent, RouterLink, FormsModule],
  templateUrl: './connection-screen.component.html',
  styleUrls: ['./connection-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionScreenComponent implements OnInit, OnDestroy {
  gameService = inject(MultiplayerGameService);
  router = inject(Router);

  sub: Subscription = Subscription.EMPTY;
  sessionId = '';

  connect() {
    this.gameService.connectToGame(this.sessionId);
  }

  ngOnInit() {
    this.sub = this.gameService.players$.pipe(skip(1)).subscribe(() => {
      this.router.navigate(['/game/lobby'], { queryParams: { sessionId: this.sessionId } });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
