import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PauseMenuComponent } from './pause-menu.component';
import { RouterModule } from '@angular/router';
import { DialogRef } from '@angular/cdk/dialog';

describe('PauseMenuComponent', () => {
  let component: PauseMenuComponent;
  let fixture: ComponentFixture<PauseMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [{ provide: DialogRef, useValue: { close: jest.fn() } }],
      imports: [PauseMenuComponent, RouterModule.forRoot([])],
    }).compileComponents();

    fixture = TestBed.createComponent(PauseMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
