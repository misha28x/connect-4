import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { DialogRef } from '@angular/cdk/dialog';
import { RouterLink } from '@angular/router';

import { ButtonComponent, CardComponent } from 'shared/ui';

import { CloseMessage, CloseMessages } from './modal-close';

@Component({
  standalone: true,
  selector: 'app-pause-menu',
  imports: [CardComponent, ButtonComponent, RouterLink],
  templateUrl: './pause-menu.component.html',
  styleUrls: ['./pause-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PauseMenuComponent {
  messages = CloseMessages;

  private dialogRef = inject(DialogRef<CloseMessage>);
  handleClose(msg: CloseMessage = this.messages.resume) {
    this.dialogRef.close(msg);
  }
}
