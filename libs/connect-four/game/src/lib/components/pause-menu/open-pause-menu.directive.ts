import { Directive, EventEmitter, HostListener, inject, OnDestroy, Output } from '@angular/core';
import { Dialog } from '@angular/cdk/dialog';
import { Subscription } from 'rxjs';

import { CloseMessage, CloseMessages } from './modal-close';

@Directive({
  selector: '[openPauseMenu]',
  standalone: true,
})
export class OpenPauseMenuDirective implements OnDestroy {
  dialog = inject(Dialog);

  @Output() opened = new EventEmitter();
  @Output() closed = new EventEmitter();
  @Output() restarted = new EventEmitter();

  private sub: Subscription = Subscription.EMPTY;

  @HostListener('click')
  async open() {
    const { PauseMenuComponent } = await import('./pause-menu.component');

    this.opened.emit();

    const ref = this.dialog.open<CloseMessage>(PauseMenuComponent);

    this.sub = ref.closed.pipe().subscribe((msg) => {
      if (msg === CloseMessages.restart) {
        this.restarted.emit();
      } else {
        this.closed.emit();
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
