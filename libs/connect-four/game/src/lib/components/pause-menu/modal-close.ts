export type CloseMessage = 'resume' | 'restart' | 'quit';
export const CloseMessages: Record<CloseMessage, CloseMessage> = {
  resume: 'resume',
  restart: 'restart',
  quit: 'quit',
};
