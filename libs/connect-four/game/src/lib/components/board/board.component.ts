import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Figure, Player, WinnSequence } from 'game';
import { GameService } from 'connect-four/models';
import { appear } from 'shared/animations';

import { MarkerComponent } from '../marker/marker.component';
import { FigureComponent } from '../figure/figure.component';
import { AnimateFigureDirective } from '../figure/animate-figure.directive';

@Component({
  selector: 'app-board',
  standalone: true,
  imports: [CommonModule, FigureComponent, MarkerComponent, AnimateFigureDirective],
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  animations: [appear],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoardComponent {
  gameService = inject(GameService);

  @Input() player!: Player;
  @Input() figures: Figure[] = [];
  @Input() sequence: WinnSequence | null = null;

  markers = this.gameService.markers;

  trackFigure(_: number, figure: Figure) {
    return `${figure.row}-${figure.col}`;
  }
}
