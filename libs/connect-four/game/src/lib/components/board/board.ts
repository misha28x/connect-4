import { clamp } from 'shared/util';

export type Sizes = 'small' | 'large';
export type BoardConfig = {
  width: number;
  gap: number;
};

export type ResponsiveConfig = Record<Sizes, BoardConfig>;

const COL_COUNT = 7;

export function getMaxWidth(config: BoardConfig) {
  return COL_COUNT * (config.width + config.gap);
}

export function getColumnOffset(col: number, { width, gap }: BoardConfig): number {
  return col * (width + gap);
}

export function calculateColumn([offset, config]: [number, BoardConfig]): number {
  const maxWidth = getMaxWidth(config);
  const { gap, width } = config;

  const normalizedOffset = clamp(offset - gap, 0, maxWidth);
  return Math.floor(normalizedOffset / (width + gap));
}

export const config: ResponsiveConfig = {
  large: {
    width: 66,
    gap: 22,
  },
  small: {
    width: 39,
    gap: 8,
  },
};
