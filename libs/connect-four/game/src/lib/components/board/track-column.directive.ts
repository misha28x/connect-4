import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  inject,
  OnDestroy,
  Output,
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { calculateColumn, config } from './board';
import {
  distinctUntilChanged,
  fromEvent,
  map,
  Observable,
  share,
  shareReplay,
  startWith,
  Subject,
  takeUntil,
  throttleTime,
  withLatestFrom,
} from 'rxjs';

const mobileBreakpoint = '(max-width: 680px)';

@Directive({
  selector: '[appTrackColumn]',
  standalone: true,
})
export class TrackColumnDirective implements AfterViewInit, OnDestroy {
  breakpointObs = inject(BreakpointObserver);
  element = inject(ElementRef).nativeElement;

  config$ = this.breakpointObs.observe([mobileBreakpoint]).pipe(
    distinctUntilChanged(),
    map(() => this.breakpointObs.isMatched(mobileBreakpoint)),
    map((isMatched) => (isMatched ? config.small : config.large)),
    shareReplay(1)
  );

  @Output() colHovered = new EventEmitter<number>();
  @Output() figurePlaced = new EventEmitter<number>();

  private destroy$ = new Subject<void>();

  ngAfterViewInit() {
    this.clickedColumn().subscribe((col) => this.figurePlaced.emit(col));
    this.hoveredColumn().subscribe((col) => this.colHovered.emit(col));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private clickedColumn() {
    return fromEvent(this.element, 'click').pipe(
      withLatestFrom(this.hoveredColumn()),
      map(([, col]) => col),
      takeUntil(this.destroy$)
    );
  }

  private hoveredColumn(): Observable<number> {
    return fromEvent<MouseEvent>(this.element, 'mousemove').pipe(
      throttleTime(1000 / 60),
      map((event) => event.offsetX),
      withLatestFrom(this.config$),
      map(calculateColumn),
      distinctUntilChanged(),
      startWith(0),
      takeUntil(this.destroy$),
      share()
    );
  }
}
