import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnIndicatorComponent } from './turn-indicator.component';
import { GameService } from "connect-four/models";
import { provideTimer } from "shared/tokens";
import { HotSeatGameService } from "connect-four/services";

describe('TurnIndicatorComponent', () => {
  let component: TurnIndicatorComponent;
  let fixture: ComponentFixture<TurnIndicatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TurnIndicatorComponent],
      providers: [{ provide: GameService, useClass: HotSeatGameService }, provideTimer()],
    }).compileComponents();

    fixture = TestBed.createComponent(TurnIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
