import { ChangeDetectionStrategy, Component, inject, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

import { filter, map, repeat, startWith } from 'rxjs';

import { GameService } from 'connect-four/models';

import { injectTimer } from 'shared/tokens';
import { SvgIconPipe } from 'shared/pipes';

@Component({
  selector: 'app-turn-indicator',
  standalone: true,
  imports: [CommonModule, SvgIconPipe],
  templateUrl: './turn-indicator.component.html',
  styleUrls: ['./turn-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TurnIndicatorComponent {
  game = inject(GameService);
  timer$ = injectTimer();

  player$ = this.game.currentPlayer$;
  image$ = this.player$.pipe(
    map((p) => `color-${p.color}`),
    startWith('color-red')
  );

  @Output() timerRunOut = this.timer$.pipe(
    filter((val) => val === 0),
    repeat({ delay: () => this.player$ })
  );
}
