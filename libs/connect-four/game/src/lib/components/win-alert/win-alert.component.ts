import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardComponent, ChipButtonComponent } from 'shared/ui';

@Component({
  selector: 'app-win-alert',
  standalone: true,
  imports: [CommonModule, CardComponent, ChipButtonComponent],
  templateUrl: './win-alert.component.html',
  styleUrls: ['./win-alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WinAlertComponent {
  @Input() winner?: string = '';
  @Output() playAgain = new EventEmitter();
}
