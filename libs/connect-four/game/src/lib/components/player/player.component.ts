import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardComponent, IconComponent } from 'shared/ui';
import { incrementAnimation } from 'shared/animations';

@Component({
  selector: 'app-player',
  standalone: true,
  imports: [CommonModule, CardComponent, IconComponent],
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [incrementAnimation],
})
export class PlayerComponent {
  @HostBinding('class')
  get playerClass() {
    return `player-${this.num}`;
  }

  @Input() num!: number;
  @Input() name = '';

  _score = 0;

  get score() {
    return this._score;
  }

  @Input()
  set score(val: number) {
    this._score = val;
    1;
    this.scoreArray.push(val);
  }

  scoreArray: number[] = [];

  get playerIcon() {
    return this.num === 1 ? 'player-one' : 'player-two';
  }
}
