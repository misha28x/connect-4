import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { delay, map, Subscription } from 'rxjs';

import { GameService } from 'connect-four/models';
import { FigureColor } from 'game';

import { ICON_RESOLVER } from 'shared/tokens';
import { config, getColumnOffset } from '../board/board';

@Component({
  selector: 'app-marker',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './marker.component.html',
  styleUrls: ['./marker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarkerComponent implements OnInit, OnDestroy {
  resolver = inject(ICON_RESOLVER);
  gameService = inject(GameService);
  cdr = inject(ChangeDetectorRef);

  @Input() markerId = 0;

  @HostBinding('class')
  color: FigureColor = 'yellow';

  @HostBinding('style.--offset.px')
  offset = 0;

  yellowIcon = this.resolver.resolve('marker-yellow');
  redIcon = this.resolver.resolve('marker-red');

  private sub = Subscription.EMPTY;

  ngOnInit(): void {
    this.sub = this.gameService.markersState$
      .pipe(
        delay(0),
        map((markers) => markers[this.markerId])
      )
      .subscribe(({ col, color }) => {
        this.color = color;
        this.offset = getColumnOffset(col, config.large);
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
