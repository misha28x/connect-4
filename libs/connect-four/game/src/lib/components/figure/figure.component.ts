import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Figure, isPartOfSequence, WinnSequence } from 'game';

@Component({
  selector: 'app-figure',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './figure.component.html',
  styleUrls: ['./figure.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FigureComponent {
  @HostBinding('class.won')
  get hasWon() {
    if (this.sequence === null) return false;
    return isPartOfSequence(this.figure, this.sequence);
  }

  @Input() figure!: Figure;
  @Input() sequence: WinnSequence | null = null;

  @HostBinding('style.grid-area')
  get area() {
    return `${this.row + 1}/${this.col + 1}/${this.row + 1}/${this.col + 1}`;
  }

  @HostBinding('class')
  get iconClass() {
    return `color-${this.figure.color}`;
  }

  get col() {
    return this.figure.col;
  }

  get row() {
    return this.figure.row;
  }
}
