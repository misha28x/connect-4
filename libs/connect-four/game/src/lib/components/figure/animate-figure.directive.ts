import { AfterContentInit, Directive, ElementRef, inject } from '@angular/core';

const g = 9.81;
const SLOW_FACTOR = 50;
// Calculation done by this formula: t = √(2 · distance / g)
// But slowed down  to make it look better
function calculateTiming(distance: number) {
  return Math.sqrt((2 * distance) / g) * SLOW_FACTOR;
}

@Directive({
  selector: '[animateFigure]',
  standalone: true,
})
export class AnimateFigureDirective implements AfterContentInit {
  element = inject(ElementRef).nativeElement;

  ngAfterContentInit() {
    requestAnimationFrame(() => {
      const distance = this.element.offsetTop;
      const duration = calculateTiming(distance);

      this.element.style.transform = `translateY(-${distance}px)`;
      this.element.style.opacity = '0.8';

      requestAnimationFrame(() => {
        this.element.style.transition = this.getTransition(duration);
        this.element.style.opacity = '1';
        this.element.style.transform = 'translateY(0)';
      });
    });
  }

  private getTransition(duration: number) {
    return `
        opacity 0.35s ease-in-out,
        transform ${duration}ms cubic-bezier(0.25, 0.46, 0.45, 0.94)
    `;
  }
}
