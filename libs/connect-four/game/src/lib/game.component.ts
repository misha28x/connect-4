import { Component, inject, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Player } from 'game';

import { GameService } from 'connect-four/models';

import { SvgIconPipe } from 'shared/pipes';
import { ChipButtonComponent } from 'shared/ui';
import { createFadeAnimation } from 'shared/animations';

import { BoardComponent } from './components/board/board.component';

import { OpenPauseMenuDirective } from './components/pause-menu/open-pause-menu.directive';
import { MarkerComponent } from './components/marker/marker.component';
import { PlayerComponent } from './components/player/player.component';
import { TurnIndicatorComponent } from './components/turn-indicator/turn-indicator.component';
import { WinAlertComponent } from './components/win-alert/win-alert.component';
import { TrackColumnDirective } from './components/board/track-column.directive';

@Component({
  selector: 'app-game',
  standalone: true,
  imports: [
    CommonModule,
    BoardComponent,
    SvgIconPipe,
    ChipButtonComponent,
    OpenPauseMenuDirective,
    MarkerComponent,
    PlayerComponent,
    TurnIndicatorComponent,
    WinAlertComponent,
    TrackColumnDirective,
  ],
  animations: [createFadeAnimation({ enterDuration: 500 })],
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnDestroy {
  gameService = inject(GameService);

  vm$ = this.gameService.vm$;

  onHover(hoverIdx: number) {
    this.gameService.updateMarkerPos(hoverIdx);
  }

  onClick(index: number) {
    this.gameService.placeFigure(index);
  }

  restart() {
    this.gameService.restart();
  }

  playAgain() {
    this.gameService.playAgain();
  }

  onTimeRunOut() {
    this.gameService.placeFigure(0);
  }

  trackPlayer(idx: number, player: Player) {
    return player.id;
  }

  ngOnDestroy() {
    this.gameService.dispose();
  }
}
