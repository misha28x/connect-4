import { Inject, Injectable, InjectionToken, OnDestroy, Optional } from "@angular/core";
import { BehaviorSubject, distinctUntilChanged, isObservable, map, Observable, of, Subject, takeUntil } from "rxjs";

type SelectConfig = { emitAlways?: boolean };

const INITIAL_STATE = new InjectionToken('Initial state for store');

export function withState<T>(initialState: T) {
  return class extends Store<T> {
    constructor() {
      super(initialState);
    }
  };
}

@Injectable()
export class Store<T> implements OnDestroy {
  private state$$: BehaviorSubject<T>;
  private _destroy$$ = new Subject<void>();

  destroyed$ = this._destroy$$.asObservable();

  constructor(@Inject(INITIAL_STATE) @Optional() initialState: T) {
    this.state$$ = new BehaviorSubject<T>(initialState);
  }

  effect<
    ProviderType,
    ObservableType = ProviderType extends Observable<infer A> ? A : ProviderType
  >(generator: (origin$: ProviderType) => Observable<unknown>) {
    const origin$ = new Subject<ObservableType>();

    generator(origin$ as ProviderType)
      .pipe(takeUntil(this.destroyed$))
      .subscribe();

    return (val: Observable<ObservableType> | ObservableType) => {
      const value$: Observable<ObservableType> = isObservable(val) ? val : of(val);
      return value$.pipe(takeUntil(this.destroyed$)).subscribe((val) => origin$.next(val));
    };
  }

  patch(newState: Partial<T> | ((state: T) => Partial<T>)) {
    const current = this.state$$.value;
    const next = typeof newState === 'function' ? newState(current) : newState;

    this.state$$.next({ ...current, ...next });
  }

  select<R = Partial<T>>(selector: (state: T) => R, { emitAlways }: SelectConfig = {}) {
    const result$ = this.state$$.pipe(map(selector));

    if (emitAlways) {
      return result$;
    }

    return result$.pipe(distinctUntilChanged());
  }

  ngOnDestroy() {
    this._destroy$$.next();
    this._destroy$$.complete();
  }
}
