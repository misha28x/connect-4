import { Observable } from 'rxjs';
import { Figure, FigureColor, Game, Player, WinnSequence } from 'game';

export abstract class GameService {
  abstract restart(): void;
  abstract dispose(): void;
  abstract playAgain(): void;
  abstract placeFigure(col: number): void;
  abstract updateMarkerPos(col: number): void;
  abstract markers: number[];
  abstract vm$: Observable<GameViewModel>;
  abstract currentPlayer$: Observable<Player>;
  abstract markersState$: Observable<MarkerState[]>;
}

export type MarkerState = {
  col: number;
  color: FigureColor;
};

export type GameViewModel = {
  currentPlayer: Player;
  players: Player[];
  winner: Player | null;
  seq: WinnSequence | null;
  figures: Figure[];
  ended: boolean;
};

export function getViewModel(game: Game): GameViewModel {
  return {
    seq: game.winner?.seq || null,
    winner: game.winner?.player || null,
    figures: game.figures,
    ended: game.ended,
    currentPlayer: game.currentPlayer,
    players: game.getPlayers(),
  };
}
