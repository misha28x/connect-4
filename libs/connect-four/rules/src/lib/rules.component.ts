import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CardComponent, IconButtonComponent, IconComponent } from 'shared/ui';

@Component({
  selector: 'app-rules',
  standalone: true,
  imports: [CommonModule, CardComponent, IconComponent, IconButtonComponent, RouterLink],
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RulesComponent {}
