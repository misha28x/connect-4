import { inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Dialog } from '@angular/cdk/dialog';

import {
  combineLatest,
  distinctUntilChanged,
  filter,
  map,
  merge,
  Observable,
  scan,
  shareReplay,
  startWith,
  switchMap,
  take,
  withLatestFrom,
} from 'rxjs';

import { GameService, GameViewModel, MarkerState } from 'connect-four/models';
import { environment } from 'connect-four/environments';
import { Figure, FigureColors, Player } from 'game';

import { ClientMessages, ServerMessages } from 'shared/api';

import { MessageSocket } from './socket.service';
import { END_NOTICE } from 'shared/tokens';

@Injectable({ providedIn: 'root' })
export class MultiplayerGameService extends MessageSocket implements GameService {
  private endNoticeFactory = inject(END_NOTICE);

  private router = inject(Router);
  private dialog = inject(Dialog);
  private route = inject(ActivatedRoute);

  private started$ = this.fromMessage(ServerMessages.started).pipe(shareReplay(1));
  private update$ = this.fromMessage(ServerMessages.update);

  private currentPlayerId$ = merge(
    this.started$.pipe(map((p) => p.currentPlayerId)),
    this.update$.pipe(map((p) => p.currentPlayerId))
  );

  closed$ = this.fromMessage(ServerMessages.closed, true).pipe(shareReplay(1));

  sessionId$ = merge(
    this.fromMessage(ServerMessages.created, (p) => p.sessionId),
    this.route.queryParams.pipe(
      map((p) => p['sessionId']),
      filter(Boolean)
    )
  ).pipe(shareReplay(1), distinctUntilChanged());

  markers = [0, 1];
  markersState$: Observable<MarkerState[]> = this.fromMessage(ServerMessages.colUpdate).pipe(
    map(({ cols }) =>
      cols.map((col, idx) => ({ col, color: idx === 0 ? FigureColors.red : FigureColors.yellow }))
    )
  );

  players$: Observable<Player[]> = merge(
    this.fromMessage(ServerMessages.started, (p) => p.players),
    this.fromMessage(ServerMessages.joined, (p) => p.players),
    this.fromMessage(ServerMessages.created, [])
  ).pipe(startWith([]), shareReplay(1));

  private ended$ = merge(
    this.fromMessage(ServerMessages.win, true),
    this.fromMessage(ServerMessages.tie, true),
    this.fromMessage(ServerMessages.started, false)
  ).pipe(startWith(false));

  private winner$ = merge(
    this.fromMessage(ServerMessages.win, (p) => p.winner),
    this.started$.pipe(map(() => null))
  ).pipe(startWith(null));

  private figures$ = this.started$.pipe(
    switchMap(() =>
      this.update$.pipe(
        map((p) => p.figure),
        filter(Boolean),
        scan((acc, figure) => {
          acc.push(figure);
          return acc;
        }, [] as Figure[]),
        startWith([])
      )
    ),
    startWith([])
  );

  currentPlayer$: Observable<Player> = this.currentPlayerId$.pipe(
    withLatestFrom(this.players$),
    map(([playerId, players]) => players.find((p) => p.id === playerId) as Player),
    filter((currentPlayer): currentPlayer is Player => Boolean(currentPlayer))
  );

  vm$: Observable<GameViewModel> = combineLatest(
    this.currentPlayer$,
    this.players$,
    this.figures$,
    this.winner$,
    this.ended$,
    (currentPlayer, players, figures, winner, ended) => {
      return {
        winner: winner?.player ?? null,
        seq: winner?.seq ?? null,
        currentPlayer,
        players,
        figures,
        ended,
      };
    }
  );

  constructor() {
    super(environment.backend);

    this.started$.subscribe(() => {
      this.router.navigate(['/game/play']);
    });

    this.closed$
      .pipe(
        filter((val) => Boolean(val)),
        filter(() => this.isInGame()),
        switchMap(async () => {
          const EndNoticeComponent = await this.endNoticeFactory();
          return this.dialog.open(EndNoticeComponent).closed;
        })
      )
      .subscribe(() => this.router.navigate(['/menu']));
  }

  dispose() {
    this.sessionId$
      .pipe(take(1))
      .subscribe((sessionId) => this.send(ClientMessages.close, { sessionId }));
  }

  updateMarkerPos(col: number): void {
    this.send(ClientMessages.col, { col });
  }

  startGame() {
    this.send(ClientMessages.start);
  }

  createGame() {
    this.send(ClientMessages.create);
  }

  connectToGame(sessionId: string) {
    this.send(ClientMessages.join, { sessionId });
  }

  placeFigure(col: number) {
    this.send(ClientMessages.turn, { col });
  }

  playAgain() {
    this.send(ClientMessages.playAgain);
  }

  restart() {
    this.send(ClientMessages.restart);
  }

  private isInGame() {
    const url = this.router.url;
    return url === '/game/play' || url === '/game/create';
  }
}
