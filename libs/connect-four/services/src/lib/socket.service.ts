import { filter, map, Observable, retry, Subject, timer } from 'rxjs';
import {
  ClientMessage,
  ClientMessageType,
  isServerMessage,
  Message,
  MessageByType,
  MessagePayload,
  ServerMessage,
  ServerMessageType,
} from 'shared/api';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Inject, Injectable, Optional } from '@angular/core';

type Fn = (...args: unknown[]) => any;
const isFunction = (val: unknown): val is Fn => typeof val === 'function';

@Injectable()
export class MessageSocket {
  private readonly serverMessages$ = new Subject<ServerMessage>();
  private readonly socket: WebSocketSubject<Message>;

  constructor(@Inject('Path') @Optional() path: string) {
    this.socket = webSocket(path);
    this.listenServerMessages();
  }

  fromMessage<T extends ServerMessageType, U = MessagePayload<T>>(
    messageType: T,
    mapperOrValue?: ((payload: Omit<MessagePayload<T>, 'type'>) => U) | U
  ): Observable<U> {
    return this.serverMessages$.pipe(
      filter((message): message is MessageByType<T> => message.type === messageType),
      map(({ type, ...message }) => message),
      map((payload) => {
        if (isFunction(mapperOrValue)) {
          return mapperOrValue(payload);
        }

        if (mapperOrValue !== undefined) return mapperOrValue;

        return payload as U;
      })
    );
  }

  send<T extends ClientMessageType, P extends MessagePayload<T>>(type: T, body?: Omit<P, 'type'>) {
    const message = { type, ...body } as ClientMessage;
    this.socket.next(message);
  }

  private listenServerMessages() {
    this.socket
      .pipe(
        retry({
          count: 3,
          delay: (error, count) => {
            // Retry with an exponential step-back
            // maxing out at 1 minute.
            return timer(Math.min(60000, 3 ^ (count * 1000)));
          },
        }),
        filter(isServerMessage)
      )
      .subscribe((message) => this.serverMessages$.next(message));
  }
}
