import { Injectable } from '@angular/core';
import { Game } from 'game';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, shareReplay, startWith } from 'rxjs/operators';

import { GameService, GameViewModel, getViewModel } from 'connect-four/models';

const trackedMethods: (keyof Game)[] = ['placeFigure', 'restart', 'playAgain'];

@Injectable({ providedIn: 'root' })
export class HotSeatGameService implements GameService {
  private col$$ = new Subject<number>();
  private game = this.createProxy();

  private gameState$$ = new Subject<GameViewModel>();

  vm$: Observable<GameViewModel> = this.gameState$$.pipe(
    startWith(getViewModel(this.game)),
    shareReplay(1)
  );
  currentPlayer$ = this.vm$.pipe(map((vm) => vm.currentPlayer));
  markersState$ = combineLatest(
    [this.col$$.asObservable() as Observable<number>, this.vm$],
    (col, { currentPlayer }) => {
      return [{ col, color: currentPlayer.color }];
    }
  );

  markers = [0];

  placeFigure(col: number) {
    this.game.placeFigure(col);
    this.updateMarkerPos(col);
  }

  restart() {
    this.game.restart();
  }

  playAgain(): void {
    this.game.playAgain();
  }

  updateMarkerPos(col: number) {
    this.col$$.next(col);
  }

  dispose() {
    this.resetGame();
    this.notifyChange();
  }

  private resetGame() {
    this.game = this.createProxy();
  }

  private notifyChange() {
    this.gameState$$.next(getViewModel(this.game));
  }

  private createProxy() {
    return new Proxy(new Game(), {
      get: (target, p, receiver) => {
        if (trackedMethods.includes(p as keyof Game)) {
          requestAnimationFrame(() => this.notifyChange());
        }

        return Reflect.get(target, p, receiver);
      },
    });
  }
}
