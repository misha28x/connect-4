import { PreloadAllModules, provideRouter, withPreloading } from '@angular/router';
import { enableProdMode, importProvidersFrom } from '@angular/core';
import { provideAnimations } from '@angular/platform-browser/animations';
import { bootstrapApplication } from '@angular/platform-browser';
import { DialogModule } from '@angular/cdk/dialog';

import { environment } from 'connect-four/environments';

import { AppComponent } from './app/app.component';
import { routes } from './app/routes';
import { END_NOTICE } from 'shared/tokens';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    provideAnimations(),
    provideRouter(routes, withPreloading(PreloadAllModules)),
    importProvidersFrom(DialogModule),
    {
      provide: END_NOTICE,
      useFactory: () => import('shared/ui').then((c) => c.EndNoticeComponent),
    },
  ],
}).catch((err) => console.error(err));
