import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  template: '<router-outlet/>',
  imports: [RouterOutlet],
  styles: [
    `
      :host {
        display: block;
        height: 100dvh;
        background-color: var(--purple);
      }
    `,
  ],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {}
