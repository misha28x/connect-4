import { inject } from '@angular/core';
import { Router, Routes } from '@angular/router';

import { map } from 'rxjs';

import { END_NOTICE, provideTimer } from 'shared/tokens';
import { GameService } from 'connect-four/models';
import { MainMenuComponent } from 'connect-four/main-menu';
import { HotSeatGameService, MultiplayerGameService } from 'connect-four/services';

export const routes: Routes = [
  { path: '', redirectTo: 'menu', pathMatch: 'full' },
  {
    path: 'menu',
    component: MainMenuComponent,
  },
  {
    path: 'game',
    children: [
      {
        path: 'lobby',
        loadComponent: () => import('connect-four/main-menu').then((c) => c.LobbyComponent),
      },
      {
        path: 'local',
        loadComponent: () => import('connect-four/game').then((c) => c.GameComponent),
        providers: [provideTimer(), { provide: GameService, useExisting: HotSeatGameService }],
      },
      {
        path: 'play',
        loadComponent: () => import('connect-four/game').then((c) => c.GameComponent),
        providers: [{ provide: GameService, useExisting: MultiplayerGameService }, provideTimer()],
        canActivate: [
          () => {
            const game = inject(MultiplayerGameService);
            const router = inject(Router);
            const players$ = game.players$;

            const redirect = router.createUrlTree(['/menu'], {
              queryParams: { mode: 'multiplayer' },
            });

            return players$.pipe(map((p) => (p.length === 2 ? true : redirect)));
          },
        ],
      },
    ],
  },
  {
    path: 'rules',
    loadComponent: () => import('connect-four/rules').then((c) => c.RulesComponent),
  },
];
