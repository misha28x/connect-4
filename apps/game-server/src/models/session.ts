import { createPlayer, Game, Player } from 'game';

export class GameSession {
  readonly game: Game;
  readonly players: string[];
  readonly cols: [number, number];

  constructor(player1: Player, player2: Player) {
    this.game = new Game(player1, player2);
    this.players = [player1.id, player2.id];
    this.cols = [0, 0];
  }

  updateCol(playerId: string, col: number) {
    const colIdx = this.players.indexOf(playerId);
    this.cols[colIdx] = col;
  }
}

export function createGameSession(player1Id: string, player2Id: string) {
  const player1 = createPlayer('Player1', player1Id);
  const player2 = createPlayer('Player2', player2Id);
  return new GameSession(player1, player2);
}
