import { ClientMessage, ClientMessageType, MessageByType, MessagePayload } from 'shared/api';

import { Notifier } from './notifier';
import { GameSession } from './session';
import { SessionManager } from './session-manager';

export const sessionMessages = ['create', 'join', 'close'] as const;

export type SessionMessageType = (typeof sessionMessages)[number];
export type GameMessageType = Exclude<ClientMessageType, SessionMessageType>;

type BaseHandler<T extends ClientMessageType, Payload> = (
  params: {
    message: MessagePayload<T>;
    notifier: Notifier;
    playerId: string;
  } & Payload
) => void;

export type SessionHandler<T extends SessionMessageType> = BaseHandler<
  T,
  { manager: SessionManager }
>;
export type GameHandler<T extends GameMessageType> = BaseHandler<T, { session: GameSession }>;

export type SessionHandlers = {
  [key in SessionMessageType]: SessionHandler<key>;
};

export type GameHandlers = {
  [key in GameMessageType]: GameHandler<key>;
};

export type Handlers = SessionHandlers & GameHandlers;

export function isSessionMessage(
  message: ClientMessage
): message is MessageByType<SessionMessageType> {
  return sessionMessages.some((t) => t === message.type);
}
