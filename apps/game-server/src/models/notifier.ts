import { ServerMessage } from 'shared/api';

export type Notifier = (receivers: string | string[], message: ServerMessage) => void;
