import { WebSocket } from 'ws';
import { getRandomId } from '../utils';

export class PlayerManager {
  private players: Map<string, WebSocket> = new Map();

  getPlayer(id: string) {
    return this.players.get(id);
  }

  addPlayer(socket: WebSocket) {
    if (socket.id) return socket;

    const playerId = getRandomId();
    this.players.set(playerId, socket);
    socket.id = playerId;

    return socket;
  }
}
