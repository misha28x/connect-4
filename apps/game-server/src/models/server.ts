import { WebSocket, WebSocketServer } from 'ws';
import { ClientMessage, parseClientMessage, ServerMessage } from 'shared/api';

import { SessionManager } from './session-manager';
import { PlayerManager } from './player-manager';
import { isSessionMessage } from './handler';
import { serializeMessage } from '../utils';
import { sessionHandlers } from '../handlers/session-handlers';
import { gameHandlers } from '../handlers/game-handlers';

export function createServer(port: string | number) {
  return new Server({ port: validatePort(port) });
}

export type Config = {
  port: number;
};

class Server {
  private readonly sessionManager: SessionManager = new SessionManager();
  private readonly playerManager: PlayerManager = new PlayerManager();
  private server!: WebSocketServer;

  constructor({ port }: Config) {
    this.server = new WebSocketServer({ port });
    this.sessionManager.onSessionCreated(this.setSession);
    this.server.on('connection', this.handleConnection);
  }

  private handleMessage(message: ClientMessage, { id, sessionId }: WebSocket) {
    const { type, ...payload } = message;
    const base = {
      message: payload as any, // Typescript can't figure it out for handler :(
      notifier: this.notifier,
      playerId: id,
    };

    if (isSessionMessage(message)) {
      const handler = sessionHandlers[message.type];
      handler({ ...base, manager: this.sessionManager });
    } else {
      const handler = gameHandlers[message.type];
      const session = this.sessionManager.getSession(sessionId);

      if (session) {
        handler({
          ...base,
          session,
        });
      }
    }
  }

  private handleConnection = (socket: WebSocket) => {
    this.playerManager.addPlayer(socket);

    socket.on('message', (data) => {
      const message = parseClientMessage(data);

      try {
        this.handleMessage(message, socket);
      } catch (e) {
        console.log(e);
      }
    });

    socket.on('close', () => this.handleClose(socket));
  };

  private handleClose({ sessionId, id }: WebSocket) {
    const closeHandler = sessionHandlers.close;
    closeHandler({
      playerId: id,
      notifier: this.notifier,
      manager: this.sessionManager,
      message: { sessionId: sessionId },
    });
  }

  private setSession = (players: string[], sessionId: string) => {
    this.server.clients.forEach((s) => {
      if (players.includes(s.id)) s.sessionId = sessionId;
    });
  };

  private notifier = (receivers: string | string[], message: ServerMessage) => {
    const receiversIds = Array.isArray(receivers) ? receivers : [receivers];
    const connections = receiversIds
      .map((id) => this.playerManager.getPlayer(id))
      .filter((c): c is WebSocket => Boolean(c));

    connections.forEach((connection) => connection.send(serializeMessage(message)));
  };
}

function validatePort(port: number | string): number {
  const portNumber = parseInt(port.toString(), 10);

  if (Number.isNaN(portNumber)) {
    throw new Error('Port is not a number');
  }

  if ([80, 443].includes(portNumber)) {
    throw new Error('Port is reserved');
  }

  return portNumber;
}
