import { createGameSession, GameSession } from './session';
import { getRandomId } from '../utils';

interface PendingGame {
  sessionId: string;
  gameCreator: string;
}

type CreatedListener = (players: string[], sessionId: string) => void;

export class SessionManager {
  private pendingGames: Map<string, PendingGame> = new Map();
  private activeGames: Map<string, GameSession> = new Map();

  private creationListener: CreatedListener[] = [];

  closeSession(sessionId: string) {
    if (this.activeGames.has(sessionId)) {
      this.activeGames.delete(sessionId);
    }
  }

  createGame(player1: string): string {
    const pendingGame = this.createPendingGame(player1);
    this.pendingGames.set(pendingGame.sessionId, pendingGame);
    return pendingGame.sessionId;
  }

  connectToGame(player2: string, sessionId: string) {
    const pendingGame = this.pendingGames.get(sessionId.toLowerCase());
    if (pendingGame) {
      const gameSession = createGameSession(pendingGame.gameCreator, player2);

      this.notifySessionCreation(gameSession, sessionId);
      this.activeGames.set(sessionId, gameSession);
      this.pendingGames.delete(sessionId);
    }
  }

  getSession(sessionId: string): GameSession | null {
    return this.activeGames.get(sessionId) || null;
  }

  onSessionCreated(cb: CreatedListener) {
    this.creationListener.push(cb);
  }

  private notifySessionCreation(session: GameSession, sessionId: string) {
    this.creationListener.forEach((cb) => cb(session.players, sessionId));
  }

  private createPendingGame(creatorId: string): PendingGame {
    return {
      sessionId: getRandomId(),
      gameCreator: creatorId,
    };
  }
}
