import { GameHandlers } from '../models/handler';
import { messages } from '../message-factory';

export const gameHandlers: GameHandlers = {
  start: ({ session, notifier }) => {
    const { game, players } = session;
    notifier(players, messages['started'](game));
  },
  turn: ({ session, message, notifier, playerId }) => {
    const { game, players } = session;

    if (game.currentPlayer.id !== playerId || game.ended) return;

    game.placeFigure(message.col);
    notifier(players, messages['update'](game));

    if (game.ended) {
      notifier(players, messages['win'](game));
    }
  },
  col: ({ message, session, notifier, playerId }) => {
    session.updateCol(playerId, message.col);
    const { players, cols } = session;
    notifier(players, { type: 'colUpdate', cols });
  },
  restart: ({ session, notifier }) => {
    const { game, players } = session;
    game.restart();
    notifier(players, messages['started'](game));
  },
  playAgain: ({ session, notifier }) => {
    const { game, players } = session;
    game.playAgain();
    notifier(players, messages['started'](game));
  },
};
