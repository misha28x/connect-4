import { SessionHandlers } from '../models/handler';

export const sessionHandlers: SessionHandlers = {
  create: ({ manager, playerId, notifier }) => {
    const session = manager.createGame(playerId);
    notifier(playerId, { type: 'created', sessionId: session });
  },
  join: ({ message, manager, notifier, playerId }) => {
    manager.connectToGame(playerId, message.sessionId);
    const createdSession = manager.getSession(message.sessionId);

    if (createdSession) {
      const { game, players } = createdSession;
      notifier(players, {
        type: 'joined',
        players: game.getPlayers(),
        sessionId: message.sessionId,
      });
    }
  },
  close: ({ manager, notifier, message }) => {
    const session = manager.getSession(message.sessionId);

    if (session) {
      const { players } = session;
      manager.closeSession(message.sessionId);
      notifier(players, { type: 'closed' });
    }
  },
};
