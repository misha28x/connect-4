import { Game } from 'game';
import { ServerMessage } from 'shared/api';

export const messages: Record<string, (game: Game) => ServerMessage> = {
  started: (game: Game) => {
    return {
      type: 'started',
      players: game.getPlayers(),
      currentPlayerId: game.currentPlayer.id,
    };
  },

  update: (game) => {
    return {
      type: 'update',
      figure: game.figures.at(-1) || null,
      currentPlayerId: game.currentPlayer.id,
    };
  },

  win: (game) => {
    return game.winner ? { type: 'win', winner: game.winner } : { type: 'tie' };
  },
} as const;
