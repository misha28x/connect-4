import { createServer } from './models/server';

const port = process.env['PORT'] ?? '8080';

createServer(port);

declare module 'ws' {
  interface WebSocket {
    id: string;
    sessionId: string;
  }
}

declare global {
  namespace NodeJs {
    interface ProcessEnv {
      PORT: string;
    }
  }
}
