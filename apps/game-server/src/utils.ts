import * as crypto from 'crypto';
import { ServerMessage } from 'shared/api';

export function getRandomId() {
  return crypto.randomUUID().substring(0, 6);
}

export function serializeMessage<T extends ServerMessage>(data: T) {
  return JSON.stringify(data);
}
